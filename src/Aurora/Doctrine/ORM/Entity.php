<?php

namespace Sysco\Aurora\Doctrine\ORM;

use \Exception;

class Entity
{

    public function __call($name, $arguments)
    {
        if (substr($name, 0, 3) === 'has') {
            $attribute = lcfirst(substr($name, 3));
            return !(null === $this->{$attribute} || empty($this->{$attribute}));
        }

        throw new Exception(sprintf('Call to undefined method %s::%s()', get_class($this), $name));
    }

}