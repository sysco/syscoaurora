<?php

namespace Sysco\Aurora\Doctrine\ORM\Controller\Plugin;

use Doctrine\ORM\Query;
use Zend\Paginator\Paginator as ZendPaginator;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class Paginator extends AbstractPlugin
{

    protected $itemCountPerPage = 10;

    /**
     * 
     * @return Paginator
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * Set the pagination options in the Plugin.
     * @param array $options
     * @return Paginator
     */
    public function setOptions(array $options = array())
    {
        if (array_key_exists('item_count_per_page', $options)) {
            $this->itemCountPerPage = $options['item_count_per_page'];
        }

        return $this;
    }

    /**
     * Returns the current page value in the pagination.
     * @return integer
     */
    public function getCurrentPage()
    {
        return (int) $this->getController()->params()->fromRoute('page', 1);
    }

    /**
     * Return the itemCountPerPage attribute.
     * @return int
     */
    public function getItemCountPerPage()
    {
        return $this->itemCountPerPage;
    }

    /**
     * 
     * @param Query $query
     * @param integer $itemCountPerPage
     * @return Paginator
     */
    public function paginate(Query $query, $itemCountPerPage = null, $useOutputWalkers = false)
    {
        $currentPage = $this->getCurrentPage();

        if (null === $itemCountPerPage) {
            $itemCountPerPage = $this->itemCountPerPage;
        }

        $offset = $itemCountPerPage * ($currentPage - 1);

        $query
                ->setMaxResults($itemCountPerPage)
                ->setFirstResult($offset);

        $ORMPaginator = new ORMPaginator($query, false);
        $ORMPaginator->setUseOutputWalkers($useOutputWalkers);

        $paginator = new ZendPaginator(new DoctrinePaginator($ORMPaginator));

        $paginator
                ->setCurrentPageNumber($currentPage)
                ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

}

