<?php

namespace Sysco\Aurora\Stdlib;

use \Exception as PhpException;

class Exception extends PhpException
{

    /**
     * 
     * @param string|array $message
     * @param type $code
     * @param type $previous
     */
    public function __construct($message, $code = 0, $previous = null)
    {
        parent::__construct(null, $code, $previous);

        $this->setMessage($message);
    }

    private function getEnvironment()
    {
        return getenv('APPLICATION_ENV') ? : 'production';
    }

    public function getMessage()
    {
        $environment = $this->getEnvironment();
        return $this->message[$environment];
    }

    public function setMessage($message)
    {
        if (is_array($this->message)) {
            if (!array_key_exists('development', $message)) {
                
            }

            if (!array_key_exists('production', $message)) {
                $message['production'] = 'An error ocurred.';
            }
        } else {
            $message = array(
                'development' => '',
                'production' => $message
            );
        }

        $this->message = $message;
    }

}
