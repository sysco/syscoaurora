<?php

namespace Sysco\Aurora\Form;

use Zend\Form\Form as ZendForm;

class Form extends ZendForm
{

    const CSRF_ELEMENT = 'csrf';

    protected $csrfElementTimeout = 600;
    protected $dependencies = array();

    public function __construct($name = null, $options = array())
    {
        if (null === $name) {
            $name = 'form-' . strtolower(str_replace('\\', '_', substr(get_class($this), 0, -4)));
        }

        if (!array_key_exists('action', $this->attributes)) {
            $this->attributes['action'] = $_SERVER['REQUEST_URI'];
        }

        parent::__construct($name, $options);

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => self::CSRF_ELEMENT,
            'options' => array(
                'csrf_options' => array(
                    'timeout' => $this->csrfElementTimeout
                )
            )
        ));
    }

    /**
     * Set options for a form. Accepted options are:
     * - prefer_form_input_filter: is form input filter is preferred?
     *
     * @param  array|Traversable $options
     * @return Element|ElementInterface
     * @throws Exception\InvalidArgumentException
     */
    public function setOptions($options)
    {
        if (isset($options['dependencies'])) {
            $this->setDependencies($options['dependencies']);
            unset($options['dependencies']);
        }

        if (isset($options['csrf_element_timeout'])) {
            $this->csrfElementTimeout = $options['csrf_element_timeout'];
            unset($options['csrf_element_timeout']);
        }

        return parent::setOptions($options);
    }

    public function setDependencies($dependencies = array())
    {
        $this->dependencies = $dependencies;
        return $this;
    }

    /**
     * Return all the dependencies of the fieldset
     * @return array
     */
    public function getDependencies()
    {
        return $this->dependencies;
    }

    /**
     * Return an specific dependency previously added
     * @param  string $name
     * @return object
     */
    public function getDependency($name)
    {
        if (array_key_exists($name, $this->dependencies)) {
            return $this->dependencies[$name];
        }

        throw new \Exception('Undefined dependency \'' . $name . '\'');
    }

    /**
     * Return the same string from imput but works for making the string detectable by Poedit or similar.
     * @param string $text
     * @return string
     */
    public function translate($text)
    {
        return $text;
    }

    /**
     * Return the fieldset of the form with the specified name.
     * @param string $name
     * @return Fieldset
     */
    public function getFieldset($name)
    {
        return $this->fieldsets[$name];
    }

}
