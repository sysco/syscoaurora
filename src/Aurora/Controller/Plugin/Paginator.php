<?php

namespace Sysco\Aurora\Controller\Plugin;

use Zend\Paginator\Adapter\Iterator;
use Zend\Paginator\Paginator as ZendPaginator;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Paginator extends AbstractPlugin
{

    protected $itemCountPerPage = 10;

    /**
     *
     * @return \Sysco\Aurora\Controller\Plugin\Paginator
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * 
     * @param array $options
     * @return \Sysco\Aurora\Controller\Plugin\Paginator
     */
    public function setOptions(array $options = array())
    {
        if (array_key_exists('item_count_per_page', $options)) {
            $this->itemCountPerPage = $options['item_count_per_page'];
        }

        return $this;
    }

    /**
     * Returns the current page value in the pagination.
     * @return integer
     */
    public function getCurrentPage()
    {
        return (int) $this->getController()->params()->fromRoute('page', 1);
    }

    /**
     * Return the itemCountPerPage attribute.
     * @return int
     */
    public function getItemCountPerPage()
    {
        return $this->itemCountPerPage;
    }

    /**
     * 
     * @param Zend\Db\ResultSet $resulSet
     * @param integer $itemCountPerPage
     * @return Zend\Paginator\Paginator
     */
    public function paginate(ResultSet $resultSet, $itemCountPerPage = null)
    {
        $currentPage = $this->getCurrentPage();

        if (null === $itemCountPerPage) {
            $itemCountPerPage = $this->itemCountPerPage;
        }

        $resultSet->buffer();

        $iteratorAdapter = new Iterator($resultSet);

        $paginator = new ZendPaginator($iteratorAdapter);
        $paginator
                ->setCurrentPageNumber($currentPage)
                ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

}
