<?php

namespace Sysco\Aurora\Doctrine\Stdlib;

use Sysco\Aurora\Stdlib\DateTime;
use Sysco\Aurora\Filter\ArrayKeyUnderscoreToCamelCase;
use Sysco\Aurora\Filter\ArrayKeyCamelCaseToUnderscore;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class Hydrator extends DoctrineHydrator
{

    /**
     * Flag defining whether array keys are underscore-separated (true) or camel case (false)
     * @var bool
     */
    protected $underscoreSeparatedKeys = true;

    /**
     * @param  bool      $underscoreSeparatedKeys
     * @return Hydrator
     */
    public function setUnderscoreSeparatedKeys($underscoreSeparatedKeys)
    {
        $this->underscoreSeparatedKeys = $underscoreSeparatedKeys;
        return $this;
    }

    /**
     * @return bool
     */
    public function getUnderscoreSeparatedKeys()
    {
        return $this->underscoreSeparatedKeys;
    }

    public function hydrate(array $data, $object)
    {
        if ($this->underscoreSeparatedKeys) {
            $filter = new ArrayKeyUnderscoreToCamelCase;
            $data = $filter->filter($data);
        }

        return parent::hydrate($data, $object);
    }

    public function extract($object)
    {
        $data = parent::extract($object);

        if ($this->underscoreSeparatedKeys) {
            $filter = new ArrayKeyCamelCaseToUnderscore;
            $data = $filter->filter($data);
        }

        return $data;
    }

    protected function handleTypeConversions($value, $typeOfField)
    {
        switch ($typeOfField) {
            case 'datetime':
            case 'time':
            case 'date':
                if (is_int($value)) {
                    $dateTime = new DateTime();
                    $dateTime->setTimestamp($value);
                    $value = $dateTime;
                } elseif (is_string($value)) {
                    $value = new DateTime($value);
                }

                break;
            default:
        }

        return $value;
    }

}
