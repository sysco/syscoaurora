<?php

namespace Sysco\Aurora\Validator;

use \Exception;
use Zend\Validator\AbstractValidator;

class DateCompare extends AbstractValidator
{

    const NOT_GREATER = 'notGreaterThan';
    const NOT_GREATER_INCLUSIVE = 'notGreaterThanInclusive';
    const NOT_LESS = 'notLessThan';
    const NOT_LESS_INCLUSIVE = 'notLessThanInclusive';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::NOT_GREATER => "The input date is not greater than '%reference%'",
        self::NOT_GREATER_INCLUSIVE => "The input date is not greater or equal than '%reference%'",
        self::NOT_LESS => "The input date is not less than '%reference%'",
        self::NOT_LESS_INCLUSIVE => "The input date is not less or equal than '%reference%'"
    );

    /**
     * @var array
     */
    protected $messageVariables = array(
        'reference' => 'reference'
    );

    /**
     * Reference value
     *
     * @var mixed
     */
    protected $reference;

    public function __construct($options)
    {
        parent::__construct($options);

        if (!array_key_exists('compare', $options)) {
            throw new Exception('Missing operator for the comparison.');
        }

        if (!array_key_exists('reference', $options)) {
            throw new Exception('Missing reference field for the comparison.');
        }
    }

    public function isValid($value, $context = array())
    {
        $referenceField = $this->getOption('reference');

        if (!is_array($context) or !isset($context[$referenceField])) {
            throw new Exception\RuntimeException(sprintf('Field "%s" missing in the context', $referenceField));
        }

        $ownValue = $this->convertToTime($value);
        $referenceValue = $this->convertToTime($context[$referenceField]);

        $this->reference = date('Y-m-d H:i', $referenceValue);

        switch ($this->getOption('compare')) {
            case 'gt':
                if (!($ownValue > $referenceValue)) {
                    $this->error(self::NOT_GREATER);
                    return false;
                }
                break;
            case 'get':
                if (!($ownValue >= $referenceValue)) {
                    $this->error(self::NOT_GREATER_INCLUSIVE);
                    return false;
                }
                break;
            case 'lt':
                if (!($ownValue < $referenceValue)) {
                    $this->error(self::NOT_LESS);
                    return false;
                }

                break;
            case 'let':
                if (!($ownValue <= $referenceValue)) {
                    $this->error(self::NOT_LESS_INCLUSIVE);
                    return false;
                }
                break;
            default:
                throw new Exception('You must specify a valid comparison operator.');
                break;
        }

        return true;
    }

    private function convertToTime($value)
    {
        /* Check notes from http://php.net/manual/en/function.strtotime.php, format disambiguation */
        $value = str_replace('/', '-', $value);
        return strtotime($value);
    }

}
