<?php

namespace Sysco\Aurora\Service;

class Service
{

    protected $dependencies = array();
    protected $translator = null;

    public function __construct(array $options = array())
    {
        $this->setOptions($options);
    }

    public function setOptions(array $options)
    {
        if (isset($options['dependencies'])) {
            $this->setDependencies($options['dependencies']);
            unset($options['dependencies']);
        }

        if (isset($options['translator'])) {
            $this->setTranslator($options['translator']);
            unset($options['translator']);
        }
    }

    public function addDependency($name, $dependency)
    {
        $this->dependencies[$name] = $dependency;
        return $this;
    }

    public function getDependency($name)
    {
        return $this->dependencies[$name];
    }

    public function getDependencies()
    {
        return $this->dependencies;
    }

    public function setDependencies(array $dependencies)
    {
        return $this->dependencies = $dependencies;
    }

    public function hasDependency($name)
    {
        return array_key_exists($name, $this->dependencies);
    }

    protected function setTranslator($translator)
    {
        $this->translator = $translator;
        return $this;
    }

    protected function getTranslator()
    {
        return $this->translator;
    }

    public static function translate($text)
    {
       if (isset($this) && null !== $this->translator) {
            return $this->translator->translate($text);
        }

        return $text;
    }

}
