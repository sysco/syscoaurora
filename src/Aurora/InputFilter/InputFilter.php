<?php

namespace Sysco\Aurora\InputFilter;

use Zend\InputFilter\InputFilter as ZFInputFilter;
use Sysco\Aurora\Filter\ArrayKeyCamelCaseToUnderscore;

class InputFilter extends ZFInputFilter
{

    /**
     * Flag defining whether array keys are underscore-separated (true) or camel case (false)
     * @var bool
     */
    protected $underscoreSeparatedKeys = false;

    /**
     * @param  bool      $underscoreSeparatedKeys
     * @return Hydrator
     */
    public function setUnderscoreSeparatedKeys($underscoreSeparatedKeys)
    {
        $this->underscoreSeparatedKeys = $underscoreSeparatedKeys;
        return $this;
    }

    /**
     * @return bool
     */
    public function getUnderscoreSeparatedKeys()
    {
        return $this->underscoreSeparatedKeys;
    }

    public function setData($data)
    {
        if ($this->underscoreSeparatedKeys) {
            $filter = new ArrayKeyCamelCaseToUnderscore;
            $data = $filter->filter($data);
        }

        return parent::setData($data);
    }

}
