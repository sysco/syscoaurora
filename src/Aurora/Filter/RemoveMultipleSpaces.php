<?php

namespace Sysco\Aurora\Filter;

use Zend\Filter\FilterInterface;

class RemoveMultipleSpaces implements FilterInterface
{

    public function filter($string)
    {
        return preg_replace('/\s+/', ' ', $string);
    }

}
