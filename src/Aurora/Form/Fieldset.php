<?php

namespace Sysco\Aurora\Form;

use Zend\Form\Fieldset as ZendFieldset;
use Sysco\Aurora\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\InputFilter\Factory as InputFilterFactory;

class Fieldset extends ZendFieldset implements InputFilterProviderInterface
{

    /**
     *
     * @var array 
     */
    protected $dependencies = array();
    
    protected $translator = null;

    /**
     * Set options for a fieldset. Accepted options are:
     * - use_as_base_fieldset: is this fieldset use as the base fieldset?
     *
     * @param  array|Traversable $options
     * @return Element|ElementInterface
     * @throws Exception\InvalidArgumentException
     */
    public function setOptions($options)
    {
        if (isset($options['dependencies'])) {
            $this->setDependencies($options['dependencies']);
            unset($options['dependencies']);
        }

        if (isset($options['translator'])) {
            $this->setTranslator($options['translator']);
            unset($options['translator']);
        }

        return parent::setOptions($options);
    }

    public function setDependencies($dependencies = array())
    {
        $this->dependencies = $dependencies;
        return $this;
    }

    /**
     * Return all the dependencies of the fieldset.
     * @return array
     */
    public function getDependencies()
    {
        return $this->dependencies;
    }

    /**
     * Return a dependency of the fieldset.
     * @param string $name
     * @return object
     * @throws \Exception
     */
    public function getDependency($name)
    {
        if (array_key_exists($name, $this->dependencies)) {
            return $this->dependencies[$name];
        }

        throw new \Exception('Undefined dependency \'' . $name . '\'');
    }

    /**
     * Add a dependency to the fieldset.
     * @param string $name
     * @param object $dependency
     * @return Fieldset
     */
    public function addDependency($name, $dependency)
    {
        $this->dependencies[$name] = $dependency;
        return $this;
    }

    /**
     * Return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array();
    }

    /**
     * Return the input filter for the fieldset based on the method getInputFilterSpecification.
     * @return InputFilter
     */
    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $inputFactory = new InputFilterFactory();

        foreach ($this->getInputFilterSpecification() as $name => $spec) {
            $input = $inputFactory->createInput($spec);
            $inputFilter->add($input, $name);
        }

        return $inputFilter;
    }

    /**
     * Return the input filter for the fieldset and set the needed Data for the validation.
     * @param array $data
     * @return InputFilter
     */
    public function getInputFilterAndSetData(array $data)
    {
        $inputFilter = $this->getInputFilter();
        return $inputFilter->setData($data);
    }

    protected function setTranslator($translator)
    {
        $this->translator = $translator;
        return $this;
    }

    protected function getTranslator()
    {
        return $this->translator;
    }

    /**
     * Return the same string from imput but works for making the string detectable by Poedit or similar.
     * @param string $text
     * @return string
     */
    public static function translate($text)
    {
        if (isset($this) && null !== $this->translator) {
            return $this->translator->translate($text);
        }

        return $text;
    }

}
