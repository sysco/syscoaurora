<?php

namespace Sysco\Aurora\Doctrine\ORM;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository as DoctrineEntityRepository;

/**
 * An EntityRepository serves as a repository for entities with generic as well as
 * business specific methods for retrieving entities.
 *
 * This class is designed for inheritance and users can subclass this class to
 * write their own repositories with business-specific methods to locate entities.
 *
 * @since   2.0
 * @author  José Carlos Chávez <jcchavezs@gmail.com>
 */
class EntityRepository extends DoctrineEntityRepository
{

    public function findByAsQuery(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
                ->select('e')
                ->from($this->getEntityName(), 'e');

        foreach ($criteria as $key => $value) {
            $qb
                    ->andWhere("e.{$key} = :{$key}")
                    ->setParameter($key, $value);
        }

        if (null !== $orderBy) {
            foreach ($orderBy as $order => $ordering) {
                $qb->addOrderBy('e.' . $order, $ordering);
            }
        }

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        if (null !== $offset) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery();
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->findByAsQuery($criteria, $orderBy, $limit, $offset)->getResult();
    }

    public function findByAsArray(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->findByAsQuery($criteria, $orderBy, $limit, $offset)->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function countBy(array $criteria = array())
    {
        $metadata = $this->getEntityManager()->getClassMetadata($this->getEntityName());

        $identifiers = $metadata->getIdentifierFieldNames();

        $id = current($identifiers);

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
                ->select("COUNT(e.{$id})")
                ->from($this->getEntityName(), 'e');

        if(!empty($criteria)){
            foreach ($criteria as $key => $value) {
                $qb
                        ->andWhere("e.{$key} = :{$key}")
                        ->setParameter($key, $value);
            }
        }

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

}
