<?php

namespace Sysco\Aurora\Stdlib\Hydrator;

use Zend\Stdlib\Hydrator\ArraySerializable as ZendArraySerializable;
use Sysco\Aurora\Filter\ArrayKeyCamelCaseToUnderscore;
use Sysco\Aurora\Filter\ArrayKeyUnderscoreToCamelCase;

class ArraySerializable extends ZendArraySerializable
{

    /**
     * Flag defining whether array keys are underscore-separated (true) or camel case (false)
     * @var bool
     */
    protected $underscoreSeparatedKeys = true;

    /**
     * @param  bool      $underscoreSeparatedKeys
     * @return ArraySerializable
     */
    public function setUnderscoreSeparatedKeys($underscoreSeparatedKeys)
    {
        $this->underscoreSeparatedKeys = $underscoreSeparatedKeys;
        return $this;
    }

    /**
     * @return bool
     */
    public function getUnderscoreSeparatedKeys()
    {
        return $this->underscoreSeparatedKeys;
    }

    /**
     * Extract values from the provided object
     *
     * Extracts values via the object's getArrayCopy() method.
     *
     * @param  object $object
     * @return array
     * @throws Exception\BadMethodCallException for an $object not implementing getArrayCopy()
     */
    public function extract($object)
    {
        try {
            $data = parent::extract($object);

            if ($this->underscoreSeparatedKeys) {
                $filter = new ArrayKeyCamelCaseToUnderscore();
                $data = $filter->filter($data);
            }

            return $data;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Hydrate an object
     *
     * Hydrates an object by passing $data to either its exchangeArray() or
     * populate() method.
     *
     * @param  array $data
     * @param  object $object
     * @return object
     * @throws Exception\BadMethodCallException for an $object not implementing exchangeArray() or populate()
     */
    public function hydrate(array $data, $object)
    {
        try {
            if ($this->underscoreSeparatedKeys) {
                $filter = new ArrayKeyUnderscoreToCamelCase();
                $data = $filter->filter($data);
            }

            return parent::hydrate($data, $object);
        } catch (Exception $e) {
            throw $e;
        }
    }

}
