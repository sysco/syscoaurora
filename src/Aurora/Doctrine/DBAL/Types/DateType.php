<?php

namespace Sysco\Aurora\Doctrine\DBAL\Types;

use Doctrine\DBAL\Types\DateType as DoctrineDateType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Sysco\Aurora\Stdlib\DateTime;

class DateType extends DoctrineDateType
{

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof DateTime) {
            return $value;
        }

        $format = $platform->getDateFormatString();

        $val = DateTime::createFromFormat('!' . $format, $value);

        if (!$val) {
            throw ConversionException::conversionFailedFormat($value, $this->getName(), $platform->getDateFormatString());
        }

        $val->setStringFormat($format);

        return $val;
    }

}
