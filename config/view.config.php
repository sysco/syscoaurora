<?php

return array(
    'factories' => array(
        'BootstrapDataTable' => function($sm) {
            $routeMatch = $sm->getServiceLocator()->get('router')->match($sm->getServiceLocator()->get('request'));
            return new Sysco\Aurora\View\Helper\Bootstrap\DataTable($routeMatch);
        },
        'BootstrapPaginator' => function($sm) {
            $routeMatch = $sm->getServiceLocator()->get('router')->match($sm->getServiceLocator()->get('request'));
            return new Sysco\Aurora\View\Helper\Bootstrap\Paginator($routeMatch);
        }
    )
);