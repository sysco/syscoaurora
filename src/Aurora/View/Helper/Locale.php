<?php

namespace Sysco\Aurora\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Locale extends AbstractHelper
{

    protected $locale;

    public function __invoke()
    {
        return $this;
    }

    public function getLocale()
    {
        if (!isset($this->locale)) {
            $this->locale = $this->getView()->plugin('translate')->getTranslator()->getLocale();
        }

        return $this->locale;
    }

    public function getLanguage()
    {
        return substr($this->getLocale(), 0, 2);
    }

    public function getCountry()
    {
        return substr($this->getLocale(), 3);
    }

    public function __toString()
    {
        return $this->getLocale();
    }

}
