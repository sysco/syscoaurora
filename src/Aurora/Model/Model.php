<?php

namespace Sysco\Aurora\Model;

class Model
{

    public function __construct(array $attributes = array())
    {
        $this->exchangeArray($attributes);
    }

    public function exchangeArray($data = array())
    {
        foreach ($data as $attribute => $value) {
            if (property_exists($this, $attribute)) {
                $this->{'set' . ucfirst($attribute)}($value);
            }
        }
    }

    public function getArrayCopy()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
    
    public function __call($name, $arguments)
    {
        switch (substr($name, 0, 3)) {
            case 'get':
                $attribute = lcfirst(substr($name, 3));
                return $this->get($attribute);

                break;
            case 'set':
                $attribute = lcfirst(substr($name, 3));
                $this->set($attribute, $arguments[0]);
                return $this;

                break;
            case 'has':
                $attribute = lcfirst(substr($name, 3));
                return $this->has($attribute);
                break;
        }

        throw new \Exception(sprintf('Undefined method \'%s\'.', $attribute));
    }

    public function set($attribute, $value)
    {
        if (!property_exists($this, $attribute)) {
            throw new \Exception(sprintf('Undefined attribute \'%s\'.', $attribute));
        }

        $this->{$attribute} = $value;
        return $this;
    }

    public function get($attribute)
    {
        if (!property_exists($this, $attribute)) {
            throw new \Exception(sprintf('Undefined attribute \'%s\'.', $attribute));
        }

        return $this->{$attribute};
    }

    public function has($attribute)
    {
        if (!property_exists($this, $attribute)) {
            throw new \Exception(sprintf('Undefined attribute \'%s\'.', $attribute));
        }

        return !(null === $this->{$attribute} || empty($this->{$attribute}));
    }

}
