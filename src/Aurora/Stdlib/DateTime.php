<?php

namespace Sysco\Aurora\Stdlib;

use \DateTime as PhpDateTime;

class DateTime extends PhpDateTime
{

    /**
     * The string format of the date.
     * @var string
     */
    protected $stringFormat = 'Y-m-d H:i:s';

    /**
     * Parse a date in a desired format
     * @param $format
     * @param $time
     * @param $object [optional]
     * 
     * @return DateTime
     */
    public static function createFromFormat($format, $time, $object = null)
    {
        if ($time) {
            if (null === $object) {
                $dateTime = parent::createFromFormat($format, $time);
            } else {
                $dateTime = parent::createFromFormat($format, $time, $object);
            }

            return new self($dateTime->format('Y-m-d H:i:s'));
        }

        return null;
    }

    public static function getInFormat($format, $time = 'now')
    {
        $dateTime = new self($time);

        return $dateTime->format($format);
    }

    /**
     * Set the stringFormat of the DateTime
     * @param type $format
     * @return \Sysco\Aurora\Stdlib\DateTime
     */
    public function setStringFormat($format)
    {
        $this->stringFormat = $format;
        return $this;
    }

    /**
     * Return the stringFormat of the DateTime
     * @return string
     */
    public function getStringFormat()
    {
        return $this->stringFormat;
    }

    /**

      ?>
     * Return Date in ISO8601 format
     *
     * @return String
     */
    public function __toString()
    {
        return $this->format($this->stringFormat);
    }

    /**
     * Return difference between $this and $now
     *
     * @param Datetime|String $now
     * @return DateInterval
     */
    public function diff($now = 'NOW', $absolute = NULL)
    {
        if (!($now instanceOf DateTime)) {
            $now = new DateTime($now);
        }

        return parent::diff($now, $absolute);
    }

    /**
     * Return Age in Years
     *
     * @param Datetime|String $now
     * @return Integer
     */
    public function calculateAge($now = 'NOW', $format = '%y')
    {
        return $this->diff($now)->format($format);
    }

}
