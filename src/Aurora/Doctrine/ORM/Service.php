<?php

namespace Sysco\Aurora\Doctrine\ORM;

use Doctrine\ORM\EntityManager;
use Sysco\Aurora\Service\Service as AuroraService;

class Service extends AuroraService
{

    protected $entityManager;

    public function setOptions(array $options)
    {
        if (isset($options['entity_manager'])) {
            $this->setEntityManager($options['entity_manager']);
            unset($options['entity_manager']);
        }

        parent::setOptions($options);
    }

    /**
     * Set the Entity Manager of the service.
     * @param EntityManager $entityManager
     * @return \Doctrine\ORM\EntityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    /**
     * Return the Entity Manager of the service.
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Persist and flush the entity.
     * @param object $entity
     * @param boolean $clear
     * @return object
     */
    protected function persist(&$entity, $clear = false)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();

        if ($clear) {
            $this->getEntityManager()->clear();
        }
    }

    protected function remove(&$entity, $clear = false)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();

        if ($clear) {
            $this->getEntityManager()->clear();
        }
    }

}
